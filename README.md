### What is Docker
Docker is an opensource containerization platform. It enables developers to pack their applications as images which consists of
* Base OS
* Dependency Libraries
* Application Runtime
* Application code

### Advantages
* Lightweight - Less memory as required things are only available
* Build once and run anywhere -  immutable
* Resource optimization - Does not block host system resources
* Portable - works same on all operating systems
* Scalable - easy to scale because the boot up time is very less
* Isolation - Docker containers are totally independent of one other

### Docker Architecture
Docker architecture follows client-server model. consists of
* Docker CLI
* Docker Daemon
* Registries
* Docker object
    * Images
    * Containers
    * Volumes </br>

When we run any command on the CLI it reaches the docker daemon. Then daemon checks whether image is available in local registry or not. If not available pulls from central registry, store it in local, create a container out of it and responds to the client.

![alt text](docker-arch.jpg)