### Docker install
1. Create EC2 RHEL instance in AWS
2. Update the repos
```
sudo yum update -y
```
3. Install docker
```
sudo amazon-linux-extras install docker -y
```
4. Start docker
```
sudo service docker start
```
5. Enable docker
```
sudo systemctl enable docker
```
6. Add user to docker group
```
sudo usermod -a -G docker ec2-user
```
Exit once and login again

### Docker commands
to get running containers
```
docker ps
```
to list all containers
```
docker ps -a
```
to pull images
```
docker pull <image-name>:<image-tag>
```
list images
```
docker images
```
create container out of image
```
docker create <image-id>
```
start the container
```
docker start <container-id>
```
to pull, create and start container, you can just run
```
docker run <image>:tag
```
run container in dethatched mode
```
docker run -d <image>:tag
```
remove container
```
docker rm -f <container-id>
```
expose container to outside world
```
docker run -d -p <host-port>:<container-port> <image>:tag
```
how to login to running container
```
docker exec -it <container-id> bash
```
to get full info of container
```
docker inspect <container-id>
```
to check the logs of container
```
docker logs <container-id>
```
to provide environment variables to container
```
docker run -d -e name=value <image>:tag
```
how to mount volume in host to the container
```
docker run -d -v <host-dir-path>:<container-path> <image>:tag
```
for example
```
docker run -d -v /home/ec2-user/mysql-data:/var/lib/mysql mysql
```